(this.webpackJsonp=this.webpackJsonp||[]).push([[327],{"0iRb":function(e,t,s){"use strict";s.r(t);var a=s("qwVQ"),r=s.n(a),n=s("4pZC"),i=s.n(n),o=(s("h8Et"),s("hAnt")),l=s("ly/8"),c=s("QslC"),h=s("6HTq"),d=s("H8gz"),u=s("W+tr"),p=s("lT7T"),m=s("CkNe"),g=s("vRHk"),f=s("yYHy"),v=s("/LsX"),_=s("t9l/"),C=s("ObLM"),S=s("YupE"),y=s("Zxcm"),b=s("iqD1"),O=(s("uHfJ"),s("R0RX"),s("0no1"),s("3R5X"),s("gdbl"),s("ujLG"),s("HaUQ"),s("ylB9"),s("XexO"),s("Cf2W")),x=s.n(O),T=s("01V3"),E=s("zLBL"),I=s("W7MF"),j=s("mphk"),A=s("V+xf");const R="/",P=[">","@","&","#"],H=Object(_.h)(Object(_.g)("CommandPalette|Type %{commandHandle} for command, %{userHandle} for user, %{projectHandle} for project, %{issueHandle} for issue, %{pathHandle} for project file or perform generic search..."),{commandHandle:">",userHandle:"@",issueHandle:"#",projectHandle:"&",pathHandle:R},!1),w={">":Object(_.g)("CommandPalette|command"),"@":Object(_.g)("CommandPalette|user (enter at least 3 chars)"),"&":Object(_.g)("CommandPalette|project (enter at least 3 chars)"),"#":Object(_.g)("CommandPalette|issue (enter at least 3 chars)"),[R]:Object(_.g)("CommandPalette|go to project file")},D={"@":"user","&":"project","#":"issue"},k=Object(_.g)("CommandPalette|Global Commands"),G=Object(_.g)("GlobalSearch|Users"),L=Object(_.g)("CommandPalette|Pages"),B=Object(_.g)("GlobalSearch|Projects"),M=Object(_.g)("GlobalSearch|Recent issues"),N=Object(_.g)("CommandPalette|Project files"),Q={"@":G,"&":B,"#":M,[R]:N};var F=s("cmjF"),$=s("JmT7"),U=s("DwQZ"),q=s("EqYD"),z={name:"CommandPaletteSearchItem",components:{GlAvatar:F.a,GlIcon:d.a},directives:{SafeHtml:$.a},props:{item:{type:Object,required:!0},searchQuery:{type:String,required:!0}},computed:{highlightedName(){return Object(U.a)(this.item.text,this.searchQuery)}},AVATAR_SHAPE_OPTION_RECT:q.b},W=s("bPvS"),Y=Object(W.a)(z,(function(){var e=this,t=e._self._c;return t("div",{staticClass:"gl-display-flex gl-align-items-center"},[void 0!==e.item.avatar_url?t("gl-avatar",{staticClass:"gl-mr-3",attrs:{src:e.item.avatar_url,"entity-id":e.item.entity_id,"entity-name":e.item.entity_name,size:e.item.avatar_size,shape:e.$options.AVATAR_SHAPE_OPTION_RECT,"aria-hidden":"true"}}):e._e(),e._v(" "),e.item.icon?t("gl-icon",{staticClass:"gl-mr-3",attrs:{name:e.item.icon}}):e._e(),e._v(" "),t("span",{staticClass:"gl-display-flex gl-flex-direction-column"},[t("span",{directives:[{name:"safe-html",rawName:"v-safe-html",value:e.highlightedName,expression:"highlightedName"}],staticClass:"gl-text-gray-900"}),e._v(" "),e.item.namespace?t("span",{directives:[{name:"safe-html",rawName:"v-safe-html",value:e.item.namespace,expression:"item.namespace"}],staticClass:"gl-font-sm gl-text-gray-500"}):e._e()])],1)}),[],!1,null,null,null).exports,V=s("UOZ+"),Z=s.n(V),J=s("lPN2"),X=s.n(J);const K=function({name:e,items:t}){return{name:e||k,items:t.filter((function({component:e}){return"invite_members"!==e}))}},ee=function(e,t){var s;if(e.push({text:t.title,keywords:t.title,icon:t.icon,href:t.link}),null!==(s=t.items)&&void 0!==s&&s.length){const s=t.items.map((function({title:e,link:s}){return{keywords:e,text:[t.title,e].join(" > "),href:s,icon:t.icon}}));e=[...e,...s]}return e},te=function(e,t){return{icon:"doc-code",text:t,href:Object(f.B)(e,t)}};var se={name:"CommandPaletteItems",components:{GlDisclosureDropdownGroup:T.a,GlLoadingIcon:E.a,SearchItem:Y},inject:["commandPaletteCommands","commandPaletteLinks","autocompletePath","searchContext","projectFilesPath","projectBlobPath"],props:{searchQuery:{type:String,required:!0},handle:{type:String,required:!0,validator:function(e){return[...P,R].includes(e)}}},data:function(){return{groups:[],loading:!1,projectFiles:[],debouncedSearch:i()((function(){switch(this.handle){case">":this.getCommandsAndPages();break;case"@":case"&":case"#":this.getScopedItems();break;case R:this.getProjectFiles()}}),v.k)}},computed:{isCommandMode(){return">"===this.handle},isPathMode(){return this.handle===R},commands(){return this.commandPaletteCommands.map(K)},links(){return this.commandPaletteLinks.reduce(ee,[])},filteredCommands(){var e=this;return this.searchQuery?this.commands.map((function({name:t,items:s}){return{name:t,items:e.filterBySearchQuery(s,"text")}})).filter((function({items:e}){return e.length})):this.commands},hasResults(){var e;return(null===(e=this.groups)||void 0===e?void 0:e.length)&&this.groups.some((function(e){var t;return null===(t=e.items)||void 0===t?void 0:t.length}))},hasSearchQuery(){var e,t;return this.isCommandMode||this.isPathMode?(null===(t=this.searchQuery)||void 0===t?void 0:t.length)>0:(null===(e=this.searchQuery)||void 0===e?void 0:e.length)>2},searchTerm(){return"#"===this.handle?"#"+this.searchQuery:this.searchQuery},filteredProjectFiles(){return this.searchQuery?this.filterBySearchQuery(this.projectFiles,"text").slice(0,20):this.projectFiles.slice(0,20)}},watch:{searchQuery:{handler(){this.debouncedSearch()},immediate:!0}},methods:{filterBySearchQuery(e,t="keywords"){return x.a.filter(e,this.searchQuery,{key:t})},async getProjectFiles(){if(!this.projectFiles.length){this.loading=!0;try{const e=await j.a.get(this.projectFilesPath);this.projectFiles=null==e?void 0:e.data.map(te.bind(null,this.projectBlobPath))}catch(e){I.a(e)}finally{this.loading=!1}}this.groups=[{name:N,items:this.filteredProjectFiles}]},getCommandsAndPages(){if(!this.searchQuery)return void(this.groups=[...this.commands]);this.groups=[...this.filteredCommands];const e=this.filterBySearchQuery(this.links);e.length&&this.groups.push({name:L,items:e})},async getScopedItems(){if(!(this.searchQuery&&this.searchQuery.length<3)){this.loading=!0;try{var e;const t=await j.a.get(function({path:e,searchTerm:t,handle:s,projectId:a}){const r=Z()({term:t,project_id:a,filter:"search",scope:D[s]},X.a);return`${e}?${Object(f.D)(r)}`}({path:this.autocompletePath,searchTerm:this.searchTerm,handle:this.handle,projectId:null===(e=this.searchContext.project)||void 0===e?void 0:e.id}));this.groups=[{name:Q[this.handle],items:t.data.map(A.a)}]}catch(e){I.a(e)}finally{this.loading=!1}}}}},ae=Object(W.a)(se,(function(){var e=this,t=e._self._c;return t("div",[e.loading?t("gl-loading-icon",{staticClass:"gl-my-5",attrs:{size:"lg"}}):e.hasResults?t("ul",{staticClass:"gl-p-0 gl-m-0 gl-list-style-none"},e._l(e.groups,(function(s,a){return t("gl-disclosure-dropdown-group",{key:a,class:{"gl-mt-0!":0===a},attrs:{group:s,bordered:""},scopedSlots:e._u([{key:"list-item",fn:function({item:s}){return[t("search-item",{attrs:{item:s,"search-query":e.searchQuery}})]}}],null,!0)})})),1):e.hasSearchQuery&&!e.hasResults?t("div",{staticClass:"gl-text-gray-700 gl-pl-5 gl-py-3"},[e._v("\n    "+e._s(e.__("No results found"))+"\n  ")]):e._e()],1)}),[],!1,null,null,null).exports,re={name:"FakeSearchInput",props:{userInput:{type:String,required:!0},scope:{type:String,required:!0,validator:function(e){return[...P,R].includes(e)}}},computed:{placeholder(){return w[this.scope]}}},ne=s("R8uv"),ie=s.n(ne),oe=s("RsUc"),le=s.n(oe),ce={insert:"head",singleton:!1},he=(ie()(le.a,ce),le.a.locals,Object(W.a)(re,(function(){var e=this,t=e._self._c;return t("div",{staticClass:"gl-display-flex gl-pointer-events-none fake-input"},[t("span",{staticClass:"gl-opacity-0",attrs:{"data-testid":"search-scope"}},[e._v(e._s(e.scope)+" ")]),e._v(" "),e.userInput?e._e():t("span",{staticClass:"gl-text-gray-500 gl-pointer-events-none",attrs:{"data-testid":"search-scope-placeholder"}},[e._v(e._s(e.placeholder))])])}),[],!1,null,"23fa2530",null).exports),de=s("Q04j"),ue={name:"GlobalSearchAutocompleteItems",i18n:{AUTOCOMPLETE_ERROR_MESSAGE:S.b},components:{GlAvatar:F.a,GlAlert:de.a,GlLoadingIcon:E.a,GlDisclosureDropdownGroup:T.a},directives:{SafeHtml:$.a},computed:{...Object(m.f)(["search","loading","autocompleteError"]),...Object(m.d)(["autocompleteGroupedSearchOptions","scopedSearchOptions"]),isPrecededByScopedOptions(){return this.scopedSearchOptions.length>1}},methods:{highlightedName(e){return Object(U.a)(e,this.search)}},AVATAR_SHAPE_OPTION_RECT:q.b},pe=Object(W.a)(ue,(function(){var e=this,t=e._self._c;return t("div",[e.loading?t("gl-loading-icon",{staticClass:"my-4",attrs:{size:"lg"}}):t("ul",{staticClass:"gl-m-0 gl-p-0 gl-list-style-none"},e._l(e.autocompleteGroupedSearchOptions,(function(s){return t("gl-disclosure-dropdown-group",{key:s.name,class:{"gl-mt-0!":!e.isPrecededByScopedOptions},attrs:{group:s,bordered:""},scopedSlots:e._u([{key:"list-item",fn:function({item:s}){return[t("div",{staticClass:"gl-display-flex gl-align-items-center"},[void 0!==s.avatar_url?t("gl-avatar",{staticClass:"gl-mr-3",attrs:{src:s.avatar_url,"entity-id":s.entity_id,"entity-name":s.entity_name,size:s.avatar_size,shape:e.$options.AVATAR_SHAPE_OPTION_RECT,"aria-hidden":"true"}}):e._e(),e._v(" "),t("span",{staticClass:"gl-display-flex gl-flex-direction-column"},[t("span",{directives:[{name:"safe-html",rawName:"v-safe-html",value:e.highlightedName(s.text),expression:"highlightedName(item.text)"}],staticClass:"gl-text-gray-900",attrs:{"data-testid":"autocomplete-item-name"}}),e._v(" "),s.value?t("span",{directives:[{name:"safe-html",rawName:"v-safe-html",value:s.namespace,expression:"item.namespace"}],staticClass:"gl-font-sm gl-text-gray-500",attrs:{"data-testid":"autocomplete-item-namespace"}}):e._e()])],1)]}}],null,!0)})})),1),e._v(" "),e.autocompleteError?t("gl-alert",{staticClass:"gl-text-body gl-mt-2",attrs:{dismissible:!1,variant:"danger"}},[e._v("\n    "+e._s(e.$options.i18n.AUTOCOMPLETE_ERROR_MESSAGE)+"\n  ")]):e._e()],1)}),[],!1,null,null,null).exports,me={name:"GlobalSearchDefaultItems",i18n:{ALL_GITLAB:S.a},components:{GlDisclosureDropdownGroup:T.a},computed:{...Object(m.f)(["searchContext"]),...Object(m.d)(["defaultSearchOptions"]),sectionHeader(){var e,t,s,a;return(null===(e=this.searchContext)||void 0===e||null===(t=e.project)||void 0===t?void 0:t.name)||(null===(s=this.searchContext)||void 0===s||null===(a=s.group)||void 0===a?void 0:a.name)||this.$options.i18n.ALL_GITLAB},defaultItemsGroup(){return{name:this.sectionHeader,items:this.defaultSearchOptions}}}},ge=Object(W.a)(me,(function(){var e=this._self._c;return e("ul",{staticClass:"gl-p-0 gl-m-0 gl-list-style-none"},[e("gl-disclosure-dropdown-group",{staticClass:"gl-mt-0!",attrs:{group:this.defaultItemsGroup,bordered:""}})],1)}),[],!1,null,null,null).exports,fe={name:"GlobalSearchScopedItems",components:{GlIcon:d.a,GlToken:u.a,GlDisclosureDropdownGroup:T.a},computed:{...Object(m.f)(["search"]),...Object(m.d)(["scopedSearchGroup"])},methods:{titleLabel(e){return Object(_.h)(Object(_.g)("GlobalSearch|in %{scope}"),{search:this.search,scope:e.scope||e.description})},getTruncatedScope:e=>Object(g.C)(e,b.h)}},ve=Object(W.a)(fe,(function(){var e=this,t=e._self._c;return t("div",[t("ul",{staticClass:"gl-m-0 gl-p-0 gl-pb-2 gl-list-style-none"},[t("gl-disclosure-dropdown-group",{staticClass:"gl-mt-0!",attrs:{group:e.scopedSearchGroup,bordered:""},scopedSlots:e._u([{key:"list-item",fn:function({item:s}){return[t("span",{staticClass:"gl-display-flex gl-align-items-center gl-line-height-24 gl-flex-direction-row gl-w-full"},[t("gl-icon",{staticClass:"gl-flex-shrink-0 gl-mr-2 gl-pt-2 gl-mt-n2",attrs:{name:"search"}}),e._v(" "),t("span",{staticClass:"gl-flex-grow-1"},[t("gl-token",{staticClass:"gl-flex-shrink-0 gl-white-space-nowrap gl-float-right",attrs:{"view-only":""}},[s.icon?t("gl-icon",{staticClass:"gl-mr-2",attrs:{name:s.icon}}):e._e(),e._v(" "),t("span",[e._v(e._s(e.getTruncatedScope(e.titleLabel(s))))])],1),e._v("\n            "+e._s(e.search)+"\n          ")],1)],1)]}}])})],1)])}),[],!1,null,null,null).exports,_e={name:"GlobalSearchModal",SEARCH_MODAL_ID:b.k,i18n:{SEARCH_GITLAB:S.v,SEARCH_DESCRIBED_BY_WITH_RESULTS:S.u,SEARCH_DESCRIBED_BY_DEFAULT:S.s,SEARCH_DESCRIBED_BY_UPDATED:S.t,SEARCH_RESULTS_LOADING:S.y,SEARCH_RESULTS_SCOPE:S.A,MIN_SEARCH_TERM:S.i},directives:{Outside:o.a,GlTooltip:l.a,GlResizeObserverDirective:c.a},components:{GlSearchBoxByType:h.a,GlobalSearchDefaultItems:ge,GlobalSearchScopedItems:ve,GlobalSearchAutocompleteItems:pe,GlIcon:d.a,GlToken:u.a,GlModal:p.a,CommandPaletteItems:ae,FakeSearchInput:he},mixins:[Object(y.a)()],computed:{...Object(m.f)(["search","loading","searchContext"]),...Object(m.d)(["searchQuery","searchOptions","scopedSearchOptions"]),searchText:{get(){return this.search},set(e){this.setSearch(e)}},searchPlaceholder(){var e;return null!==(e=this.glFeatures)&&void 0!==e&&e.commandPalette?H:S.v},showDefaultItems(){return!this.searchText},searchTermOverMin(){var e;return(null===(e=this.searchText)||void 0===e?void 0:e.length)>b.n},showScopedSearchItems(){return this.searchTermOverMin&&this.scopedSearchOptions.length>1},searchResultsDescription(){return this.showDefaultItems?Object(_.h)(this.$options.i18n.SEARCH_DESCRIBED_BY_DEFAULT,{count:this.searchOptions.length}):this.searchTermOverMin?this.loading?this.$options.i18n.SEARCH_RESULTS_LOADING:Object(_.h)(this.$options.i18n.SEARCH_DESCRIBED_BY_UPDATED,{count:this.searchOptions.length}):this.$options.i18n.MIN_SEARCH_TERM},searchBarClasses(){return{[b.f]:this.searchTermOverMin}},showScopeHelp(){return this.searchTermOverMin&&!this.isCommandMode},searchBarItem(){var e;return null===(e=this.searchOptions)||void 0===e?void 0:e[0]},infieldHelpContent(){var e,t;return(null===(e=this.searchBarItem)||void 0===e?void 0:e.scope)||(null===(t=this.searchBarItem)||void 0===t?void 0:t.description)},infieldHelpIcon(){var e;return null===(e=this.searchBarItem)||void 0===e?void 0:e.icon},scopeTokenTitle(){return Object(_.h)(this.$options.i18n.SEARCH_RESULTS_SCOPE,{scope:this.infieldHelpContent})},searchTextFirstChar(){var e;return null===(e=this.searchText)||void 0===e?void 0:e.trim().charAt(0)},isCommandMode(){var e;return(null===(e=this.glFeatures)||void 0===e?void 0:e.commandPalette)&&(P.includes(this.searchTextFirstChar)||this.searchContext.project&&this.searchTextFirstChar===R)},commandPaletteQuery(){var e;return this.isCommandMode?null===(e=this.searchText)||void 0===e?void 0:e.trim().substring(1):""}},methods:{...Object(m.c)(["setSearch","fetchAutocompleteOptions","clearAutocomplete"]),getAutocompleteOptions:i()((function(e){this.isCommandMode||(e?this.fetchAutocompleteOptions():this.clearAutocomplete())}),v.k),getTruncatedScope:e=>Object(g.C)(e,b.h),observeTokenWidth({contentRect:{width:e}}){var t,s,a;const r=null===(t=this.$refs)||void 0===t||null===(s=t.searchInputBox)||void 0===s||null===(a=s.$el)||void 0===a?void 0:a.querySelector("input");r&&(r.style.paddingRight=e+b.e+"px")},getFocusableOptions(){var e;return Array.from((null===(e=this.$refs.resultsList)||void 0===e?void 0:e.querySelectorAll(b.m))||[])},onKeydown(e){const{code:t,target:s}=e;let a=!0;const r=this.getFocusableOptions();if(r.length<1)return;const n=s.matches(b.j);if(t===C.g)this.focusItem(0,r);else if(t===C.d)this.focusItem(r.length-1,r);else if(t===C.b){if(n)return;if(0===r.indexOf(s))return void this.focusSearchInput();this.focusNextItem(e,r,-1)}else t===C.a?this.focusNextItem(e,r,1):t===C.f?this.$refs.searchModal.close():a=!1;a&&e.preventDefault()},focusSearchInput(){this.$refs.searchInput.$el.querySelector("input").focus()},focusNextItem(e,t,s){const{target:a}=e,n=t.indexOf(a),i=r()(n+s,0,t.length-1);this.focusItem(i,t)},focusItem(e,t){var s;this.nextFocusedItemIndex=e,null===(s=t[e])||void 0===s||s.focus()},submitSearch(){var e;(null===(e=this.search)||void 0===e?void 0:e.length)<=b.n||Object(f.T)(this.searchQuery)},onSearchModalShown(){this.$emit("shown")},onSearchModalHidden(){this.searchText="",this.$emit("hidden")}},SEARCH_INPUT_DESCRIPTION:b.i,SEARCH_RESULTS_DESCRIPTION:b.l},Ce=Object(W.a)(_e,(function(){var e=this,t=e._self._c;return t("gl-modal",{ref:"searchModal",attrs:{"modal-id":e.$options.SEARCH_MODAL_ID,"hide-header":"","hide-footer":"","hide-header-close":"",scrollable:"","body-class":"gl-p-0!","modal-class":"global-search-modal",centered:!1},on:{shown:e.onSearchModalShown,hide:e.onSearchModalHidden}},[t("form",{staticClass:"gl-relative gl-rounded-base gl-w-full",class:e.searchBarClasses,attrs:{role:"search","aria-label":e.searchPlaceholder,"data-testid":"global-search-form"}},[t("div",{staticClass:"gl-p-1 gl-relative"},[t("gl-search-box-by-type",{ref:"searchInput",attrs:{id:"search",role:"searchbox","data-testid":"global-search-input",autocomplete:"off",placeholder:e.searchPlaceholder,"aria-describedby":e.$options.SEARCH_INPUT_DESCRIPTION,borderless:""},on:{input:e.getAutocompleteOptions,keydown:[function(t){return!t.type.indexOf("key")&&e._k(t.keyCode,"enter",13,t.key,"Enter")?null:(t.stopPropagation(),t.preventDefault(),e.submitSearch.apply(null,arguments))},e.onKeydown]},model:{value:e.searchText,callback:function(t){e.searchText=t},expression:"searchText"}}),e._v(" "),e.showScopeHelp?t("gl-token",{directives:[{name:"gl-resize-observer-directive",rawName:"v-gl-resize-observer-directive",value:e.observeTokenWidth,expression:"observeTokenWidth"}],staticClass:"in-search-scope-help gl-sm-display-block gl-display-none",attrs:{"view-only":"",title:e.scopeTokenTitle}},[e.infieldHelpIcon?t("gl-icon",{staticClass:"gl-mr-2",attrs:{"aria-label":e.infieldHelpContent,name:e.infieldHelpIcon,size:16}}):e._e(),e._v("\n        "+e._s(e.getTruncatedScope(e.sprintf(e.$options.i18n.SEARCH_RESULTS_SCOPE,{scope:e.infieldHelpContent})))+"\n      ")],1):e._e(),e._v(" "),t("span",{staticClass:"gl-sr-only",attrs:{id:e.$options.SEARCH_INPUT_DESCRIPTION,role:"region"}},[e._v("\n        "+e._s(e.$options.i18n.SEARCH_DESCRIBED_BY_WITH_RESULTS)+"\n      ")]),e._v(" "),e.isCommandMode?t("fake-search-input",{staticClass:"gl-absolute",attrs:{"user-input":e.commandPaletteQuery,scope:e.searchTextFirstChar}}):e._e()],1),e._v(" "),t("span",{staticClass:"gl-sr-only",attrs:{role:"region","data-testid":e.$options.SEARCH_RESULTS_DESCRIPTION,"aria-live":"polite","aria-atomic":"true"}},[e._v("\n      "+e._s(e.searchResultsDescription)+"\n    ")]),e._v(" "),t("div",{ref:"resultsList",staticClass:"global-search-results gl-overflow-y-auto gl-w-full gl-pb-2",attrs:{"data-testid":"global-search-results"},on:{keydown:e.onKeydown}},[e.isCommandMode?t("command-palette-items",{attrs:{"search-query":e.commandPaletteQuery,handle:e.searchTextFirstChar}}):[e.showDefaultItems?t("global-search-default-items"):[e.showScopedSearchItems?t("global-search-scoped-items"):e._e(),e._v(" "),t("global-search-autocomplete-items")]]],2),e._v(" "),e.searchContext?[e.searchContext.group?t("input",{attrs:{type:"hidden",name:"group_id"},domProps:{value:e.searchContext.group.id}}):e._e(),e._v(" "),e.searchContext.project?t("input",{attrs:{type:"hidden",name:"project_id"},domProps:{value:e.searchContext.project.id}}):e._e(),e._v(" "),e.searchContext.group||e.searchContext.project?[t("input",{attrs:{type:"hidden",name:"scope"},domProps:{value:e.searchContext.scope}}),e._v(" "),t("input",{attrs:{type:"hidden",name:"search_code"},domProps:{value:e.searchContext.code_search}})]:e._e(),e._v(" "),t("input",{attrs:{type:"hidden",name:"snippets"},domProps:{value:e.searchContext.for_snippets}}),e._v(" "),t("input",{attrs:{type:"hidden",name:"repository_ref"},domProps:{value:e.searchContext.ref}})]:e._e()],2)])}),[],!1,null,null,null);t.default=Ce.exports},DwQZ:function(e,t,s){"use strict";s.d(t,"a",(function(){return i}));s("uHfJ"),s("R0RX"),s("h8Et");var a=s("Cf2W"),r=s.n(a),n=s("BYh8");function i(e,t="",s="<b>",a="</b>"){if(!e)return"";if(!t)return e;const i=Object(n.b)(e.toString(),{ALLOWED_TAGS:[]}),o=r.a.match(i,t.toString());return i.split("").map((function(e,t){return o.includes(t)?`${s}${e}${a}`:e})).join("")}},ObLM:function(e,t,s){"use strict";s.d(t,"f",(function(){return a})),s.d(t,"e",(function(){return r})),s.d(t,"c",(function(){return n})),s.d(t,"a",(function(){return i})),s.d(t,"b",(function(){return o})),s.d(t,"d",(function(){return l})),s.d(t,"g",(function(){return c}));const a="Escape",r="Enter",n="Backspace",i="ArrowDown",o="ArrowUp",l="End",c="Home"},RsUc:function(e,t,s){(e.exports=s("ZSrL")(!1)).push([e.i,"\n.fake-input[data-v-23fa2530] {\n  top: 12px;\n  left: 33px;\n}\n",""])},Zxcm:function(e,t,s){"use strict";t.a=function(){return{inject:{glFeatures:{from:"glFeatures",default:function(){return{}}}}}}}}]);
//# sourceMappingURL=global_search_modal.1c2ee532.chunk.js.map